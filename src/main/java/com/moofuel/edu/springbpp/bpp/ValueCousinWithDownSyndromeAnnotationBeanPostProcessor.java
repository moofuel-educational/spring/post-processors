package com.moofuel.edu.springbpp.bpp;

import com.moofuel.edu.springbpp.annotations.ValueCousinWithDownSyndrome;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;

/**
 * @author Дмитрий
 * @since 20.12.2017
 */
@Component
public class ValueCousinWithDownSyndromeAnnotationBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        final Class<?> beanClass = bean.getClass();
        final Field[] declaredFields = beanClass.getDeclaredFields();
        for (Field field : declaredFields) {
            final boolean annotationPresent = field.isAnnotationPresent(ValueCousinWithDownSyndrome.class);
            if (annotationPresent) {
                field.setAccessible(true);
                ReflectionUtils.makeAccessible(field);
                ReflectionUtils.setField(field, bean, "I'm really a cousin with Down syndrome!");
            }
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        return bean;
    }
}
