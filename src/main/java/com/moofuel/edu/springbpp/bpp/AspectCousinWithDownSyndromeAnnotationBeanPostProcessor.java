package com.moofuel.edu.springbpp.bpp;

import com.moofuel.edu.springbpp.annotations.AspectCousinWithDownSyndrome;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Дмитрий
 * @since 20.12.2017
 */
@Component
public class AspectCousinWithDownSyndromeAnnotationBeanPostProcessor implements BeanPostProcessor {

    private Map<String, Class> memory = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        final Class<?> beanClass = bean.getClass();
        final Method[] declaredMethods = beanClass.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            final boolean annotationPresent = declaredMethod.isAnnotationPresent(AspectCousinWithDownSyndrome.class);
            if (annotationPresent) {
                this.memory.put(beanName, beanClass);
            }
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        final Class originalClass = memory.get(beanName);
        if (originalClass == null) {
            return bean;
        }
        final Method[] declaredMethods = originalClass.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            final boolean annotationPresent = declaredMethod.isAnnotationPresent(AspectCousinWithDownSyndrome.class);
            if (annotationPresent) {
                return Proxy.newProxyInstance(originalClass.getClassLoader(),
                        originalClass.getInterfaces(),
                        (proxy, method, args) -> {
                    if (method.getName().equals(declaredMethod.getName())) {
                        final Object retVal = method.invoke(bean, args);
                        return "Starting " + retVal + "(no)";
                    }
                    return method.invoke(bean, args);
                });
            }
        }
        return bean;
    }
}
