package com.moofuel.edu.springbpp.listeners;

import com.moofuel.edu.springbpp.annotations.AfterContextRefreshInvoker;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @author Дмитрий
 * @since 21.12.2017
 */
@Component
public class AfterContextRefreshInvokerHandlerApplicationListener implements ApplicationListener<ContextRefreshedEvent> {


    private ConfigurableListableBeanFactory factory;

    public AfterContextRefreshInvokerHandlerApplicationListener(ConfigurableListableBeanFactory factory) {
        this.factory = factory;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        final ApplicationContext context = event.getApplicationContext();
        final String[] names = context.getBeanDefinitionNames();
        for (String name : names) {
            final BeanDefinition beanDefinition = factory.getBeanDefinition(name);
            final String beanClassName = beanDefinition.getBeanClassName();
            if (beanClassName != null) {
                try {
                    final Class<?> originalClass = Class.forName(beanClassName);
                    final Method[] declaredMethods = originalClass.getDeclaredMethods();
                    for (Method method : declaredMethods) {
                        if (method.isAnnotationPresent(AfterContextRefreshInvoker.class)) {
                            final Object bean = context.getBean(name);
                            final Method currentMethod = bean.getClass().getMethod(method.getName(),
                                    method.getParameterTypes());
                            currentMethod.invoke(bean);
                        }
                    }
                } catch (Exception e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }
}
