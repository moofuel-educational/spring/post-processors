package com.moofuel.edu.springbpp.components.impl;

import com.moofuel.edu.springbpp.annotations.AspectCousinWithDownSyndrome;
import com.moofuel.edu.springbpp.components.Dummy;
import org.springframework.stereotype.Component;

/**
 * @author Дмитрий
 * @since 20.12.2017
 */
@Component
public class AspectDummyComponent implements Dummy {
    @Override
    @AspectCousinWithDownSyndrome
    public String doADummyAction() {
        return "Doing a dummy action... \nDone!";
    }
}
