package com.moofuel.edu.springbpp.components.impl;

import com.moofuel.edu.springbpp.annotations.AfterContextRefreshInvoker;
import com.moofuel.edu.springbpp.components.RefreshDummy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Дмитрий
 * @since 21.12.2017
 */
@Component
@Slf4j
public class AfterContextRefreshDummyComponent implements RefreshDummy {

    @Override
    @AfterContextRefreshInvoker
    public String doADummyAction() {
        final String s = "Doing dummy action!";
        log.info(s);
        return s;
    }
}
