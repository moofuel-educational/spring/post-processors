package com.moofuel.edu.springbpp.components.impl;

import com.moofuel.edu.springbpp.annotations.ValueCousinWithDownSyndrome;
import lombok.Getter;
import org.springframework.stereotype.Component;

/**
 * @author Дмитрий
 * @since 20.12.2017
 */
@Component
@Getter
public class ValueDummyComponent {

    @ValueCousinWithDownSyndrome
    private String springIReallyNeedToInjectValueHere;
}
