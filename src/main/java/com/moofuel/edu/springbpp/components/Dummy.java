package com.moofuel.edu.springbpp.components;

/**
 * @author Дмитрий
 * @since 20.12.2017
 */
public interface Dummy {

    String doADummyAction();
}
