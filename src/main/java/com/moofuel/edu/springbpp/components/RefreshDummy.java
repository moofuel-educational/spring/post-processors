package com.moofuel.edu.springbpp.components;

/**
 * @author Дмитрий
 * @since 21.12.2017
 */
public interface RefreshDummy {

    String doADummyAction();
}
