package com.moofuel.edu.springbpp.listeners;

import com.moofuel.edu.springbpp.components.impl.AfterContextRefreshDummyComponent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.verify;

/**
 * @author Дмитрий
 * @since 21.12.2017
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class AfterContextRefreshInvokerHandlerApplicationListenerTest {

    @SpyBean
    private AfterContextRefreshDummyComponent component;

    @Test
    public void onApplicationEvent() {
        verify(component).doADummyAction();
    }
}