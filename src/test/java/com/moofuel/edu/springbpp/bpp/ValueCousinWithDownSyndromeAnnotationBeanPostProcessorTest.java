package com.moofuel.edu.springbpp.bpp;

import com.moofuel.edu.springbpp.components.impl.ValueDummyComponent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Дмитрий
 * @since 20.12.2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ValueCousinWithDownSyndromeAnnotationBeanPostProcessorTest {

    private final static String TEST_PHRASE = "I'm really a cousin with Down syndrome!";

    @Autowired
    private ApplicationContext context;

    @Test
    public void postProcessBeforeInitialization() {
        final ValueDummyComponent bean = context.getBean(ValueDummyComponent.class);
        final String actual = bean.getSpringIReallyNeedToInjectValueHere();
        assertThat(actual)
                .as("The value should be injected")
                .isEqualToIgnoringCase(TEST_PHRASE);
    }
}