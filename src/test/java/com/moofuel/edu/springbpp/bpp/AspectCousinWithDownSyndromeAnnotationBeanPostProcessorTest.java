package com.moofuel.edu.springbpp.bpp;

import com.moofuel.edu.springbpp.components.Dummy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Дмитрий
 * @since 20.12.2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AspectCousinWithDownSyndromeAnnotationBeanPostProcessorTest {

    private static final String TEST_RESULT = "Starting Doing a dummy action... \nDone!(no)";
    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void postProcessAfterInitialization() {
        final Dummy bean = applicationContext.getBean(Dummy.class);
        final String actual = bean.doADummyAction();
        assertThat(actual)
                .as("Doing dummy action must be surrounded with proxy")
                .isEqualToIgnoringCase(TEST_RESULT);
    }
}
